/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;


import domain.DatosEntidad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author kevin
 */
public class datos implements Idatos {
    
    private static final String SQLSELECTPORID = "SELECT ID_Tienda, NombreTienda, Colonia,"
            + "Calle, ID_Usuario, ProductoDeTienda FROM datos WHERE ID_Tienda = ?";
    
    private static final String SQLSELECT = "SELECT ID_Tienda, NombreTienda, Colonia,"
            + "Calle, ID_Usuario, ProductoDeTienda FROM datos";
    
    private static final String SQLINSERT = "INSERT INTO datos(NombreTienda, Colonia,"
            + "Calle, ID_Usuario, ProductoDeTienda) VALUES (?,?,?,?,?)";
    
    private static final String SQLDELETE = "DELETE FROM datos WHERE ID_Tienda = ?";
    
    private static final String SQLUPDATE = "UPDATE datos "
            + "SET NombreTienda = ?,"
            + "Colonia = ?,"
            + "Calle = ?"
            + "ID_Usuario = ?"
            + "ProductoDeTienda = ?"
            + "WHERE ID_Tienda = ?";

    @Override
    public List<DatosEntidad> seleccionar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insertar(DatosEntidad usuario) throws SQLException {
        //To change body of generated methods, choose Tools | Templates.
    Connection conn = null;
        PreparedStatement stmt = null;
        int filasAfectadas = 0;

        try {
            conn = Conexion.getConnection();             
            
            stmt = conn.prepareStatement(SQLINSERT);    
            stmt.setString(1, usuario.getNombreTienda());
            stmt.setString(2, usuario.getColonia());
            stmt.setString(3, usuario.getCalle());
            stmt.setInt(4, usuario.getID_Usuario());
            stmt.setString(5, usuario.getProductoDeTienda());
            
            filasAfectadas = stmt.executeUpdate();         
        } catch (SQLException e) {
            e.printStackTrace(System.out);
        }
        finally {           
            stmt.close();
            conn.close();
        }
        return filasAfectadas;  
    
    }

    @Override
    public int eliminar(DatosEntidad usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DatosEntidad buscar(DatosEntidad usuario) {
       Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = Conexion.getConnection();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

        try {
            stmt = conn.prepareStatement(SQLSELECTPORID);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

        try {
            stmt.setInt(1, usuario.getID_Usuario());
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

        try {
            rs = stmt.executeQuery();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

        try {
            // validar
            if (rs.next()) {
                // Leer valores del registro
                String NombreTienda = rs.getString("NombreTienda");
                String Colonia = rs.getString("Colonia");
                String Calle = rs.getString("Calle");
                int ID_Usuario = rs.getInt("ID_Usuario");
                String ProductoDeTienda = rs.getString("ProductoDeTienda");
                

                // Pasar variables al objeto de retorno
                usuario.setNombreTienda(NombreTienda);
                usuario.setColonia(Colonia);
                usuario.setCalle(Calle);
                usuario.setID_Usuario(ID_Usuario);
                usuario.setProductoDeTienda(ProductoDeTienda);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        try {
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        try {
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        return usuario;
    }

    @Override
    public int actualizar(DatosEntidad usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
