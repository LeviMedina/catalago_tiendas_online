/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author kevin
 */
public class UsuariosEntidad {
    private int ID_Usuario;
    private String login;
    private String contraseña;
    private String Correo;
    private String NombreDeUsuario;
   
    
    public UsuariosEntidad(){
        
    }
    
    public UsuariosEntidad(int ID_Usuario, String login, String contraseña, String Correo, String NombreDeUsuario){
        this.ID_Usuario = ID_Usuario;
        this.login = login;
        this.contraseña = contraseña;
        this.Correo = Correo;
        this.NombreDeUsuario = NombreDeUsuario;
    }
    
    public UsuariosEntidad(String login, String contraseña, String Correo, String NombreDeUsuario){
        this.login = login;
        this.contraseña = contraseña;
        this.Correo = Correo;
        this.NombreDeUsuario = NombreDeUsuario;
        
    }
    
    public UsuariosEntidad(int ID_Usuario){
        this.ID_Usuario = ID_Usuario;
    }

    public int getID_Usuario() {
        return ID_Usuario;
    }

    public void setID_Usuario(int ID_Usuario) {
        this.ID_Usuario = ID_Usuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getNombreDeUsuario() {
        return NombreDeUsuario;
    }

    public void setNombreDeUsuario(String NombreDeUsuario) {
        this.NombreDeUsuario = NombreDeUsuario;
    }
    
    @Override
    public String toString() {
        return "usuarios{" + "ID_Usuario=" + ID_Usuario + ", login=" + login + ", contraseña=" + contraseña + ", Correo=" + Correo + ", NombreDeUsuario=" + NombreDeUsuario + '}';
    }
}
