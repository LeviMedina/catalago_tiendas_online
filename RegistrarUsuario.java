/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;


import Datos.usuarios;
import Datos.IUsuarios;
import domain.UsuariosEntidad;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author kevin
 */
public class RegistrarUsuario extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Registro de usuario</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Respuesta del ServletAlumno en: " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // processRequest(request, response);
        String accion = request.getParameter("accion");
        switch (accion) {
            
            case "agregar":
                IUsuarios alumnoa = new usuarios();
                UsuariosEntidad alumnohtmla = new UsuariosEntidad();

                // No lleva id por el auto incrementable
                alumnohtmla.setLogin(request.getParameter("usuario"));
                alumnohtmla.setContraseña(request.getParameter("contraseña"));
                alumnohtmla.setCorreo(request.getParameter("correo"));
                alumnohtmla.setNombreDeUsuario(request.getParameter("nombre"));

                String mensaje = "";
                
                try {
                    if (alumnoa.insertar(alumnohtmla) != 1) {
                        mensaje = "El usuario NO pudo ser agregado";
                    } else {
                        mensaje = "El usuario fue agregado correctamente";
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace(System.out);
                }

                
                response.setContentType("text/html;charser=UTF-8");
                try (PrintWriter out = response.getWriter()) {
                    /* TODO output your page here. You may use following sample code. */
                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>Servlet ServletAlumno</title>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<h1>Resultado: </h1>");
                    out.println(mensaje);

                    out.println("</body>");
                    out.println("</html>");
                }
                
                break;

            default:
                throw new AssertionError();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet de Usuario";
    }// </editor-fold>
    
}
